@tool
extends Node2D

@export var go:bool:
	set(b):
		go = false
		test()

@export var stores:Dictionary

func test():
	#Flip this boolean to test
	if true:
		print("Make the Store resource")
		var store = Store.new("store1")

		print("Add some data to it")
		store.add("id001", Vector2(1,2), aResource.new())
		stores["board1"] = store

		notify_property_list_changed()

	print("Show something from within")
	print(stores.board1.stuff.id001.position)

	# Also look at Inspector...
	#  Show something from within
	#  SCRIPT ERROR: Invalid get index 'position'
	#  (on base: 'Resource ()').
