class_name Store
extends Resource

@export var id:String
@export var stuff:Dictionary

func _init(_id:String) -> void:
	id = _id

func add(
			_id,
			_position,
			_resource):
	var stf = Stuff.new(
			_id,
			_position,
			_resource)
	stuff[_id] = stf

class Stuff extends Resource:
	@export var id:String
	@export var position:Vector2
	@export var resource:Resource

	func _init(
			_id,
			_position,
			_resource)->void:
		id = _id
		position = _position
		resource = _resource
